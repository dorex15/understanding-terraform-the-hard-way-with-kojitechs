
# #TERRAFORM BLOCK
# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "~> 4.0"
#     }
#   }
# }

# # Configure the AWS Provider
# provider "aws" {
#   region = var.aws_provider
# }

# # Create a VPC
# resource "aws_vpc" "dorex123" {
#   cidr_block = var.aws_vpc_cidr

#   tags = {
#     Name = "dorex123"
#   }
# }

# #Creating Ec2
# resource "aws_instance" "Dorex" {
#   ami           = var.ami_id
#   instance_type = var.aws_instance

#   tags = {
#     Name = "Dorex"
#   }
# }

# ##Assingment
# #output the instance_id or ip(private or public)

# #creating Subnet
# resource "aws_subnet" "pub_subnet_1" {
#   vpc_id     = local.aws_vpc
#   cidr_block = var.aws_pub_subnet_cidr[0]
#   availability_zone =data.aws_availability_zones.available.names[0]
#   tags = {
#     Name = "pub_subnet_1"
#   }
# }

# resource "aws_subnet" "pub_subnet_2" {
#   vpc_id     = local.aws_vpc
#   cidr_block = var.aws_pub_subnet_cidr[1]
#   availability_zone =data.aws_availability_zones.available.names[1]
#   tags = {
#     Name = "pub_subnet_2"
#   }
# }

# resource "aws_subnet" "pri_subnet_1" {
#   vpc_id     = local.aws_vpc
#   cidr_block = var.aws_pri_subnet_cidr_1
#   availability_zone =data.aws_availability_zones.available.names[1]
#   tags = {
#     Name = "pri_subnet_1"
#   }
# }

# resource "aws_subnet" "pri_subnet_2" {
#   vpc_id     = local.aws_vpc
#   cidr_block = var.aws_pri_subnet_cidr_2
#   availability_zone = data.aws_availability_zones.available.names[1]

#   tags = {
#     Name = "pri_subnet_2"
#   }
# }

# # Declare the data source
# data "aws_availability_zones" "available" {
#   state = "available"
# }