
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.aws_provider
}
data "aws_availability_zones" "available" {
    state = "available"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = var.my_modules_vpc

  azs             = slice(data.aws_availability_zones.available.names, 0,3)
  private_subnets = var.vpc_pri_sub_modules
  public_subnets  = var.vpc_pub_sub_modules
  enable_nat_gateway = false
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
