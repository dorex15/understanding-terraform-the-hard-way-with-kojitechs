

###VARIABLES(TYPES OF VARIABLES......) # STRING
##""##
###SHOULD BE DESCRIPTIVE IN NAMING VARIABLE
#CREATING A VARIABLE ===VAR
#var is a function



variable "ami_id" {
  type        = string
  description = "ami_id"
  default     = "ami-089a545a9ed9893b6"
}


variable "aws_instance" {
  type        = string
  description = "aws_instance-type"
  default     = "t2.micro"
}

variable "aws_provider" {
  type        = string
  description = "aws_provider-type"
  default     = "us-east-2"
}

variable "aws_vpc_cidr" {
  type        = string
  description = "vpc_cidr"
  default     = "10.20.0.0/16"
}

variable "aws_pub_subnet_cidr" {
  type        = list
  description = "pub_subnet_1"
  default     = ["10.20.1.0/24", "10.20.3.0/24"]
}

# variable "aws_pub_subnet_cidr_2" {
#   type        = string
#   description = "pub_subnet_2"
#   default     = "10.20.3.0/24"
# }

variable "aws_pri_subnet_cidr_1" {
  type        = string
  description = "pri_subnet_1"
  default     = "10.20.0.0/24"
}

variable "aws_pri_subnet_cidr_2" {
  type        = string
  description = "pri_subnet_1"
  default     = "10.20.2.0/24"
}

#variable for modules

variable "my_modules_vpc" {
  type        = string
  description = "the_vpc_modules"
  default     = "10.0.0.0/16"
}


variable "vpc_pri_sub_modules" {
  type        = list
  description = "lists of pri_subnet_modules"
  default     = ["10.0.1.0/24", "10.0.2.0/24","10.0.3.0/24"]
}


variable "vpc_pub_sub_modules" {
  type        = list
  description = "lists of pub_subnet_modules"
  default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}
     